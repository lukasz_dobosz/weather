var routesConfig = function ($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/");

  $stateProvider
    .state('weather', {
        url: "/",
        templateUrl: "./app/weather/weather.html",
        controller: 'WeatherController',
        controllerAs: 'vm',
        resolve: {
          cities: function () {
            return [
              {name: 'Select a city'},
              {name: 'Chicago', zip: '60601', tz: 'America/Chicago', image: 'assets/chicago.jpg'},
              {name: 'New York', zip: '10007', tz: 'America/New_York', image: 'assets/ny.jpg'},
              {name: 'Los Angeles', zip: '90067', tz: 'America/Los_Angeles', image: 'assets/la.jpg'}
            ]
          }
        }

      }
    );
};

angular
  .module('app')
  .config(routesConfig);