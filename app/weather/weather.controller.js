var WeatherController = function (weatherService, weatherFactory, cities) {
  var vm = this;

  vm.weatherService = weatherService;
  vm.weatherData = null;
  vm.selectOptions = cities;
  vm.selectedOption = vm.selectOptions[0];

  vm.onSubmit = onSubmit;
  vm.getWeatherSuccess = getWeatherSuccess;
  vm.getWeatherError = getWeatherError;

  function onSubmit(selectedOption) {
    vm.loading = true;

    vm.weatherService.getWeather(selectedOption.zip)
      .then(vm.getWeatherSuccess, vm.getWeatherError);
  }

  function getWeatherSuccess(data) {
    changeBgImage();
    vm.weatherData = prepareDataForView(data.data);

    vm.loading = false
  }

  function getWeatherError() {
    vm.loading = false;
  }

  function changeBgImage() {
    vm.bgImage = vm.selectedOption.image;
  }

  function prepareDataForView(data) {
    //add icon src property
    data.item.condition.iconSrc = weatherFactory.getIconByWeatherCode(data.item.condition.code);
    
    return data;
  }
};

WeatherController.$inject = ['weatherService', 'weatherFactory', 'cities'];

angular
  .module('app.weather', ['app.weatherUtils'])
  .controller('WeatherController', WeatherController);