var weatherService = function ($http, config) {
  this.getWeather = getWeather;

  function getWeather(zip) {
    var requestParams = {
      zip: zip
    };
    return $http({
      url: config.api.url,
      method: "GET",
      params: requestParams
    });
  }
};

weatherService.$inject = ['$http', 'config'];

angular
  .module('app.weatherUtils')
  .service('weatherService', weatherService);