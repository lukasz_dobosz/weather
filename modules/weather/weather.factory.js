var weatherFactory = function () {

  var weatherIconSrc = {
    windy: 'icon/windy.png',
    cloudy: 'icon/cloudy.png',
    rainy: 'icon/rainy.png',
    partlyCloudy: 'icon/partly-cloudy.png',
    rainMixed: 'icon/rain-mixed.png',
    snowFall: 'icon/snow-fall.png',
    sunny: 'icon/sun.png',
    thunderstorm: 'icon/thunderstorm.png',
    wind: 'icon/wind.png',
    night: 'icon/night.png'
  };

  var codeIconDependencies = {
    0: 'wind', //tornado
    1: 'wind', //tropical storm
    2: 'wind', //hurricane
    3: 'wind', //severe thunderstorms
    4: 'thunderstorm', //thunderstorms
    5: 'rainMixed', //mixed rain and snow
    6: 'rainMixed', //mixed rain and sleet
    7: 'rainMixed',// mixed snow and sleet
    8: 'snowFall', // freezing drizzle
    9: 'rainy', //drizzle
    10: 'rainMixed', //freezing rain
    11: 'rainy', //showers
    12: 'rainy', //showers
    13: 'snowFall', //snow flurries
    14: 'snowFall', //light snow showers
    15: 'snowFall', //blowing snow
    16: 'snowFall', //snow
    17: 'rainy', //hail
    18: 'rainMixed', //sleet
    19: 'wind', //dust
    20: 'wind', //foggy
    21: 'wind', //haze
    22: 'cloudy', //smoky
    23: 'wind', //blustery
    24: 'windy', //windy
    25: 'snowFall', //cold
    26: 'cloudy', //cloudy
    27: 'cloudy', //mostly cloudy(night)
    28: 'cloudy', //mostly cloudy (day)
    29: 'partlyCloudy', //partly cloudy (night)
    30: 'partlyCloudy', //artly cloudy (day)
    31: 'night', //clear(night)
    32: 'sunny', //sunny
    33: 'night', //fair(night)
    34: 'sunny', //fair(day)
    35: 'rainy', //mixed rain and hail
    36: 'sunny', //hot
    37: 'thunderstorm', //isolated thunderstorms
    38: 'thunderstorm', //scattered thunderstorms
    39: 'thunderstorm', //scattered thunderstorms
    40: 'rainy', //scattered showers
    41: 'snowFall', //heavy snow
    42: 'snowFall', //scattered snow showers
    43: 'snowFall', //heavy snow
    44: 'partlyCloudy', //partly cloudy
    45: 'thunderstorm', //thundershowers
    46: 'snowFall', //snow showers
    47: 'thunderstorm', //isolated thundershowers
    3200: 'sunny' //unavailable
  };

  var getIconByWeatherCode = function (weatherCode) {
    if (!_.has(codeIconDependencies, weatherCode)) {
      return;
    }
    var iconKey = codeIconDependencies[weatherCode];
    if (!_.has(weatherIconSrc, iconKey)) {
      return;
    }
    return weatherIconSrc[iconKey];
  };

  return {getIconByWeatherCode: getIconByWeatherCode};
};

angular
  .module('app.weatherUtils')
  .factory('weatherFactory', weatherFactory);