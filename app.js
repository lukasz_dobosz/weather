angular
  .module('app', ['ui.router', 'app.weather', 'app.weatherUtils', 'app.sunPosition',
    'app.atmosphereBox', 'app.conditionBox', 'app.forecastBox'])
  .run(runBlock);

function runBlock() {
  moment.tz.add([
    'America/Chicago|CST CDT|60 50|0101|1Lzm0 1zb0 Op0',
    'America/New_York|EST EDT|50 40|0101|1Lz50 1zb0 Op0',
    'America/Los_Angeles|PST PDT|80 70|0101|1Lz50 1zb0 Op0'
  ]);
}
