var forecastBoxController = angular.noop;

angular
  .module('app.forecastBox', [])
  .controller('forecastBoxController', forecastBoxController);