var forecastBoxDirective = function () {
  return {
    restrict: 'E',
    templateUrl: 'components/forecast-box/forecast-box.html',
    scope: {
      forecasts: '=',
      temperatureUnit: '='
    },
    controller: 'forecastBoxController',
    controllerAs: 'vm',
    bindToController: true
  };

};

angular
  .module('app.forecastBox')
  .directive('forecastBox', forecastBoxDirective);
