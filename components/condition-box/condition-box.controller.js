var conditionBoxController = function (config) {
  var vm = this;
  vm.getFormattedDate = getFormattedDate;

  function getFormattedDate(date) {
    var srcFormat = config.dateFormat.conditionSrc;
    var dstFormat = config.dateFormat.view;

    return moment(date, srcFormat).format(dstFormat);
  }
};

conditionBoxController.$inject = ['config'];

angular
  .module('app.conditionBox', [])
  .controller('conditionBoxController', conditionBoxController);