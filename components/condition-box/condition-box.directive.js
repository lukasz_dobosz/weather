var conditionBoxDirective = function () {
    return {
        restrict: 'E',
        templateUrl: 'components/condition-box/condition-box.html',
        scope: {
            temperature: "=",
            conditionText: "=",
            conditionIcon: "=",
            temperatureUnit: "=",
            date: "="
        },
        controller: 'conditionBoxController',
        controllerAs: 'vm',
        bindToController: true
    };
};

angular
    .module('app.conditionBox')
    .directive('conditionBox', conditionBoxDirective);
