var atmosphereBoxController = angular.noop;

angular
    .module('app.atmosphereBox', [])
    .controller('atmosphereBoxController', atmosphereBoxController);