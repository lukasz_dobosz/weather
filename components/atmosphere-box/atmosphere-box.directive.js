var atmosphereBoxDirective = function () {
    return {
        restrict: 'E',
        templateUrl: 'components/atmosphere-box/atmosphere-box.html',
        scope: {
            humidity: '=',
            pressure: '=',
            rising: '=',
            visibility: '=',
            pressureUnit: '=',
            distanceUnit: '='

        },
        controller: 'atmosphereBoxController',
        controllerAs: 'vm',
        bindToController: true
    };

};

angular
    .module('app.atmosphereBox')
    .directive('atmosphereBox', atmosphereBoxDirective);
