var sunPositionController = angular.noop;

angular
  .module('app.sunPosition', [])
  .controller('sunPositionController', sunPositionController);
