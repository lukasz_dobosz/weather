var sunPositionDirective = function ($window, config) {
  return {
    restrict: 'E',
    templateUrl: 'components/sun-position/sun-position.html',
    scope: {
      sunrise: "@",
      sunset: "@",
      timezone: '@'
    },
    link: link,
    controller: 'sunPositionController',
    controllerAs: 'vm',
    bindToController: true
  };


  function link(scope, element) {

    angular.element($window).bind('resize', function () {
      animateSunPosition(scope.vm.sunrise, scope.vm.sunset, scope.vm.timezone, element);
    });

    scope.$watch('vm.sunrise', function () {
      animateSunPosition(scope.vm.sunrise, scope.vm.sunset, scope.vm.timezone, element);
    });
  }

  function animateSunPosition(sunrise, sunset, timezone, element) {
    var percentagePos = getPercentagePosition(sunrise, sunset, timezone);
    setSunPosition(element, percentagePos);
  }

  function getPercentagePosition(sunrise, sunset, timezone) {

    var localNow = moment.tz(timezone).format(config.dateFormat.default);
    var localHour = moment.tz(timezone).hours();

    var sunriseHour = moment(localNow + " " + sunrise, config.dateFormat.sun).hours();
    var sunsetHour = moment(localNow + " " + sunset, config.dateFormat.sun).hours() + 1;

    return Math.round((localHour - sunriseHour ) / (sunsetHour - sunriseHour) * 100);
  }

  function setSunPosition(element, position) {
    var pathBgWrapper = angular.element(element[0].querySelector('.path-bg-wrapper'));
    var path = angular.element(element[0].querySelector('.path'));
    var pathBg = angular.element(element[0].querySelector('.path-bg'));

    pathBg.css('width', path[0].offsetWidth + "px");
    pathBgWrapper.css({width: position + '%'});
  }
};

sunPositionDirective.$inject = ['$window', 'config'];

angular
  .module('app.sunPosition')
  .directive('sunPosition', sunPositionDirective);
