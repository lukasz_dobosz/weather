var config = function () {
  return {
    api:{
      url: 'http://localhost:3000'
    },
    dateFormat: {
      default: 'YYYY-MM-DD',
      sun: 'YYYY-MM-DD H:mm a',
      view: 'MM.DD.YYYY HH:mm a',
      conditionSrc: 'ddd, DD MMM YYYY HH:mm a'
    }
  };
};

angular
  .module('app')
  .factory('config', config);